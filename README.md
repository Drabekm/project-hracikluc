# Project Hracíkluč

DIY gameboy s cimermanovským jménem!

Srdcem konzole je raspberry pi zero a malý LCD display, konzole je napájená z baterie takže je přenosná stejně jako původní gameboy ale narozdííl od něj umí hrát i hry z jiných konzolí ze stejné éry díky retropie. 

# Hardware
- CPU: 1GHz BCM2835
- GPU: Videocore IV
- RAM: 512MB
- Display: 320x240 (ILI9341 SPI display)
- Baterie: 1000 mAh

## Roadmap
- [x] Zvukový výstup
- [ ] Zesilovač a filtr 
- [ ] Připojit display
- [ ] Napájení z baterie
- [ ] Softstart
- [ ] Namodelovat case
- [ ] Nabindovat tlačítka
- [ ] Namodelovat D-pad
- [ ] Optimalizovat bootup